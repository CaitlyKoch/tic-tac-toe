class Player:

    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

class TicTacToe:

    board = ["-", "-", "-", "-", "-", "-", "-", "-", "-"]

    def __init__(self):
        pass

    def startGame(self):
        # initialises everything
        self.moves = 1
        self.player1 = Player(input("Player one name:"), mark="X")
        self.player2 = Player(input("Player two name:"), mark="0")
        print(self.player1.name, "is playing against", self.player2.name)
        print("", self.board[0], self.board[1], self.board[2], "\n", self.board[3], self.board[4], self.board[5], "\n", self.board[6], self.board[7], self.board[8])
        self.currentPlayer = self.player1
        self.boardFull(self)

    def changeTurns(self):
        if self.currentPlayer == self.player1:
            self.currentPlayer = self.player2
        else:
            self.currentPlayer = self.player1
        self.boardFull(self)

    #checkboard for win/ tie
    def setBoard(self, choice):
        target = choice
        self.board[target] = self.currentPlayer.mark
        print("", self.board[0], self.board[1], self.board[2], "\n", self.board[3], self.board[4], self.board[5], "\n", self.board[6], self.board[7], self.board[8])
        try:
            for i in range(3):
                a = i + 3
                b = i + 6
                # check win in vertical (starting points can only be 1, 4, 7)
                if self.board[i] == self.currentPlayer.mark and self.board[a] == self.currentPlayer.mark and self.board[b] == self.currentPlayer.mark:
                    self.endGame(self)
                # check win in horizontal (starting points can only be 1, 2, 3)
            for i in range(3):
                i = i * 3
                a = i + 1
                b = i + 2
                if self.board[i] == self.currentPlayer.mark and self.board[a] == self.currentPlayer.mark and self.board[b] == self.currentPlayer.mark:
                    self.endGame(self)
                # check win in diagonal
            if self.board[0] == self.currentPlayer.mark and self.board[4] == self.currentPlayer.mark and self.board[8] == self.currentPlayer.mark:
                self.endGame(self)
            elif self.board[2] == self.currentPlayer.mark and self.board[4] == self.currentPlayer.mark and self.board[6] == self.currentPlayer.mark:
                self.endGame(self)
        except Exception as e:
            print(e)
        self.changeTurns(self)

    def boardFull(self):
            if "-" not in self.board:
                self.endGame(self)
            else:
                self.chooseSpot(self)

    def chooseSpot(self):
            print("Your turn,", self.currentPlayer.name)
            print("Round", self.moves)
            self.moves = self.moves + 1
            choice = int(input("Choose a spot:"))
            choice = choice - 1
            try:
                if 0 <= int(choice) < 9:
                    if self.board[choice] == "-":
                        self.setBoard(self, choice)
                    else:
                        print("Spot already taken.")
                        self.chooseSpot(self)
                else:
                    print("Choice must be a number between 1 and 9.")
                    self.chooseSpot(self)
            except Exception as e:
                print(e)
                self.chooseSpot(self)

    def endGame(self):
        if "-" in self.board:
            print("You won,", self.currentPlayer.name + ".")
        else:
            print("Idiots")
        rerun = input("Play again? (y/n)")
        # start again
        if rerun == "y":
            self.clearBoard(self)
        else:
            print("Okay bye.")
            quit()

    def clearBoard(self):
        print("\n" + "------- New Game -------\n" )
        self.board = ["-", "-", "-", "-", "-", "-", "-", "-", "-"]
        self.startGame(self)

TicTacToe.startGame(TicTacToe)
